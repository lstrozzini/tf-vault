terraform {
  backend "s3" {
    bucket = "ls-terraform-lab"
    key  = "terraform/states"
    region = "us-east-1"
  }
}

