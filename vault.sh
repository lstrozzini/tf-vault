#!/bin/bash 
sudo apt-get update -y &&
curl -fsSL https://apt.releases.hashicorp.com/gpg | sudo apt-key add - &&
sudo apt-add-repository "deb [arch=amd64] https://apt.releases.hashicorp.com $(lsb_release -cs) main" &&
sudo apt-get update -y && sudo apt-get install vault -y
sudo vault server -dev
export VAULT_ADDR='http://127.0.0.1:8200'
