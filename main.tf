provider "aws" {
	region = "us-west-2"
}

resource "aws_instance" "vault" {
	ami = "ami-06e54d05255faf8f6"
	instance_type = "t2.micro"
    key_name = "terraform"
    availability_zone = "us-west-2c"
	user_data = file("vault.sh")
    security_groups = ["tf-lab",
]
	tags = {
		Name = "Vault"	
	}
}
